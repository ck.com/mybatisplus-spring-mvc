package com.baomidou.springmvc.core.user.model;


import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.springmvc.common.base.model.BaseEntity;

import java.io.Serializable;

/**
 * 系统用户表
 */
@TableName("sys_user")
public class User extends BaseEntity<Long> {

    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户名
     */
    private String name;

    /**
     * 用户年龄
     */
    private Integer age;

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public User setAge(Integer age) {
        this.age = age;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return id;
    }
}