package com.baomidou.springmvc.core.user.mapper;

import com.baomidou.springmvc.common.base.dao.BaseDao;
import com.baomidou.springmvc.core.user.model.User;

/**
 * User 表数据库控制层接口
 */
public interface UserMapper extends BaseDao<User, Long> {

}