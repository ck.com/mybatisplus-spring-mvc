package com.baomidou.springmvc.core.user.service.impl;

import com.baomidou.springmvc.common.base.service.impl.BaseServiceImpl;
import com.baomidou.springmvc.core.user.model.User;
import com.baomidou.springmvc.core.user.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * User 表数据服务层接口实现类
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements IUserService {


}