package com.baomidou.springmvc.core.user.service;

import com.baomidou.springmvc.common.base.service.BaseService;
import com.baomidou.springmvc.core.user.model.User;

/**
 * User 表数据服务层接口
 */
public interface IUserService extends BaseService<User, Long> {


}