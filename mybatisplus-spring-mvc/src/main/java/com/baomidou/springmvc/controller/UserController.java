package com.baomidou.springmvc.controller;

import com.baomidou.springmvc.common.controller.BaseController;
import com.baomidou.springmvc.common.result.ResultTo;
import com.baomidou.springmvc.common.shiro.ShiroUtil;
import com.baomidou.springmvc.core.user.model.User;
import com.baomidou.springmvc.core.user.service.IUserService;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chenkai
 * 用户controller
 */
@RestController
public class UserController extends BaseController {
    @Autowired
    public IUserService userService;

    @PostMapping("/login")
    public ResultTo Login(String username, String password) {
        try {
            ShiroUtil.getSubject().login(new UsernamePasswordToken(username, password));
        } catch (UnknownAccountException e) {
            return ResultTo.error(e.getMessage());
        } catch (IncorrectCredentialsException e) {
            return ResultTo.error(e.getMessage());
        } catch (LockedAccountException e) {
            return ResultTo.error(e.getMessage());
        } catch (AuthenticationException e) {
            return ResultTo.error("账户验证失败");
        }
        return new ResultTo();
    }

}
