package com.baomidou.springmvc.common.base.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.common.base.model.BaseEntity;
import com.baomidou.springmvc.common.base.dao.BaseDao;

import java.io.Serializable;

/**
 * BaseServiceImpl
 * Created by chenkai on 2017/4/28.
 */
public class BaseServiceImpl<T extends BaseEntity<PK>, PK extends Serializable> extends ServiceImpl<BaseDao<T, PK>, T> {
}
