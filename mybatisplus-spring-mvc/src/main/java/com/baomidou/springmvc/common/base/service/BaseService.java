package com.baomidou.springmvc.common.base.service;


import com.baomidou.mybatisplus.service.IService;
import com.baomidou.springmvc.common.base.model.BaseEntity;

import java.io.Serializable;

/**
 * BaseService
 * Created by chenkai on 2017/4/28.
 */
public interface BaseService<T extends BaseEntity<PK>, PK extends Serializable> extends IService<T> {
}
