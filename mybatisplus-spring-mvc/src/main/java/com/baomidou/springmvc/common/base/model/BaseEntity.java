package com.baomidou.springmvc.common.base.model;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;

/**
 * 基础entity
 * Created by chenkai on 2017/4/28.
 */
public abstract class BaseEntity<PK extends Serializable> extends Model implements Serializable {
    private static final long serialVersionUID = 1L;

}
