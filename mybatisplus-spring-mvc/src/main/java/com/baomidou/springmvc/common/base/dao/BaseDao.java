package com.baomidou.springmvc.common.base.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.springmvc.common.base.model.BaseEntity;

import java.io.Serializable;

/**
 * BaseDao
 * Created by chenkai on 2017/4/28.
 */
public interface BaseDao<T extends BaseEntity<PK>, PK extends Serializable> extends BaseMapper<T>{
}
