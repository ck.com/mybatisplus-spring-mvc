package com.baomidou.springmvc.common.result;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 */
public class ResultTo extends HashMap<String, Object> {
	private static final long serialVersionUID = 1L;
	
	public ResultTo() {
		put("code", 200);
	}
	
	public static ResultTo error() {
		return error(500, "未知异常，请联系管理员");
	}
	
	public static ResultTo error(String msg) {
		return error(500, msg);
	}
	
	public static ResultTo error(int code, String msg) {
		ResultTo resultTo = new ResultTo();
		resultTo.put("code", code);
		resultTo.put("msg", msg);
		return resultTo;
	}

	public static ResultTo setData(String msg) {
		ResultTo resultTo = new ResultTo();
		resultTo.put("msg", msg);
		return resultTo;
	}
	
	public static ResultTo setData(Map<String, Object> map) {
		ResultTo resultTo = new ResultTo();
		resultTo.putAll(map);
		return resultTo;
	}
	
	public static ResultTo setData() {
		return new ResultTo();
	}

	public ResultTo put(String key, Object value) {
		super.put(key, value);
		return this;
	}
}
